package edu.com.asus.computershop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.com.asus.computershop.Class.Categories;
import edu.com.asus.computershop.ComputerModel;
import edu.com.asus.computershop.R;

/**
 * Created by ASUS on 6/12/2017.
 */

public class MyCategoriesAdapter extends RecyclerView.Adapter<MyCategoriesAdapter.MyViewHolder> {
    public List<Categories> categorieslist ;
    public Context context  ;
    public MyCategoriesAdapter (List<Categories> list, Context context){
        this.categorieslist = list ;
        this.context = context ;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_categories, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Categories categories = categorieslist.get(position);
        Picasso.with(context).load("http://10.0.3.2/android_ass/images/"+categories.image).into(holder.imgicon);

    }

    @Override
    public int getItemCount() {
        return categorieslist.size();
    }

    public  class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imgicon ;
        String file ;
        public MyViewHolder(final View itemView) {
            super(itemView);

            imgicon = (ImageView) itemView.findViewById(R.id.imgcategories);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    Log.d("ooooo" , String.valueOf(pos));
                    if (pos == 0){
                       file = "asus.php";
                    }
                    else if (pos == 1){
                        file = "msi.php";
                    }
                    else if (pos == 2){
                        file = "dell.php";
                    }
                    else if (pos == 3){
                        file = "lenovo.php";
                    }
                    else if (pos == 4){
                        file = "apple.php";
                    }
                    else if (pos == 5){
                        file = "acer.php";
                    }
                    Intent intent = new Intent(context , ComputerModel.class);
                    intent.putExtra("file_url", file);
                    context.startActivity(intent);


                }
            });
        }
    }
}
