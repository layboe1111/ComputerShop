package edu.com.asus.computershop;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import edu.com.asus.computershop.Class.Computer;

public class ComputerDetail extends AppCompatActivity {
    TextView tvDescription , tvPrice;
    String description ="";
    ImageView ivComputerModel ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        ivComputerModel = (ImageView) findViewById(R.id.ivComputerModel);

        Computer computer = (Computer) getIntent().getSerializableExtra("computerModel");
        if (computer!= null){
            Picasso.with(ComputerDetail.this).load("http://10.0.3.2/android_ass/images/"+computer.getImage()).into(ivComputerModel);
            tvPrice.setText("Price : "+computer.getPPrice());
            if (!computer.getCpu().equals("")){
                description += "CUP : "+computer.getCpu().toString()+"\n";
            }if (!computer.getRam().equals("")){
                description += "RAM : "+computer.getRam().toString()+"\n" ;
            }if (!computer.getHdd().equals("")){
                description += "HDD : "+computer.getHdd().toString()+"\n" ;
            }if (!computer.getVga1().equals("")){
                description += "VGA1 : "+ computer.getVga1().toString()+"\n" ;
            }if (!computer.getVga2().equals("")){
                description += "VGA2 : "+computer.getVga2().toString()+"\n" ;
            }if (!computer.getBattery().equals("")){
                description += "Battery : "+computer.getBattery().toString()+"\n" ;
            }if (!computer.getWeight().equals("")){
                description += "Weight : "+computer.getWeight().toString()+"\n" ;
            }if (!computer.getDisplay().equals("")){
                description += "Display : "+computer.getDisplay().toString()+"\n" ;
            }if (!computer.getOtherDes().equals("")){
                description += computer.getOtherDes().toString()+"\n" ;
            }
        }
        tvDescription.setText(description);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
