package edu.com.asus.computershop.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import edu.com.asus.computershop.Class.Computer;
import edu.com.asus.computershop.ComputerDetail;
import edu.com.asus.computershop.R;

/**
 * Created by ASUS on 6/13/2017.
 */

public class MyComputerModelAdapter extends RecyclerView.Adapter<MyComputerModelAdapter.Myholder> {
    private Context context ;
    private List<Computer> computerList ;

    public MyComputerModelAdapter(List<Computer> computerList, Context context ){
        this.computerList = computerList ;
        this.context = context ;
    }
    @Override
    public Myholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_computer_model ,parent ,  false);
        return new Myholder(view);
    }

    @Override
    public void onBindViewHolder(Myholder holder, int position) {
        Computer computer = computerList.get(position);
        holder.tvPrice.setText(computer.getPPrice());
        holder.tvModel.setText(computer.getPName());
        Picasso.with(context).load("http://10.0.3.2/android_ass/images/"+computer.getImage()).into(holder.imgComputer);
    }

    @Override
    public int getItemCount() {
        return computerList.size();
    }

    public class Myholder extends RecyclerView.ViewHolder{
        ImageView imgComputer ;
        TextView tvPrice , tvModel ;
        public Myholder(View itemView) {
            super(itemView);
            imgComputer = (ImageView) itemView.findViewById(R.id.imgcomputer);
            tvPrice = (TextView) itemView.findViewById(R.id.tvprice);
            tvModel = (TextView) itemView.findViewById(R.id.tvmodel);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Computer computer = computerList.get(getAdapterPosition());
                    Intent i = new Intent(context , ComputerDetail.class);
                    i.putExtra("computerModel",computer);
                    context.startActivity(i);
                }
            });
        }
    }
}
