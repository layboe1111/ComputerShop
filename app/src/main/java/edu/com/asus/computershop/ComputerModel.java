package edu.com.asus.computershop;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kosalgeek.android.json.JsonConverter;

import java.util.ArrayList;
import java.util.List;

import edu.com.asus.computershop.Class.Computer;
import edu.com.asus.computershop.adapter.MyComputerModelAdapter;

public class ComputerModel extends AppCompatActivity {
    static String geturl ;
    RecyclerView rvComputerModel ;
    MyComputerModelAdapter adapter ;
    List<Computer> list ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computer_model);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        Bundle b = getIntent().getExtras() ;
        if (b!= null) {
            geturl = b.getString("file_url");
        }
       // Toast.makeText(this, geturl, Toast.LENGTH_SHORT).show();
        list = new ArrayList<Computer>();
        rvComputerModel = (RecyclerView) findViewById(R.id.rvComputerModel);
        rvComputerModel.setLayoutManager(new StaggeredGridLayoutManager(2 , StaggeredGridLayoutManager.VERTICAL));

        adapter= new MyComputerModelAdapter(list , this);
        rvComputerModel.setAdapter(adapter);
        LoadComputerModel();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


    public  void LoadComputerModel(){
        final ProgressDialog dialog = new ProgressDialog(ComputerModel.this);
        dialog.setMessage("Loading data....");
        dialog.show();
        String url = "http://10.0.3.2/android_ass/api/"+geturl;

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                list.addAll( new JsonConverter<Computer>().toArrayList(response , Computer.class));
                dialog.dismiss();
                adapter.notifyDataSetChanged();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                Toast.makeText(ComputerModel.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(ComputerModel.this);
        requestQueue.add(request);
    }

}
