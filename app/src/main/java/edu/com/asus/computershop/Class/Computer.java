package edu.com.asus.computershop.Class;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ASUS on 6/13/2017.
 */

public class Computer implements Serializable {


    @SerializedName("id")
    private String id;
    @SerializedName("p_name")
    private String pName;
    @SerializedName("p_price")
    private String pPrice;
    @SerializedName("image")
    private String images;
    @SerializedName("c_name")
    private String cName;
    @SerializedName("type")
    private String type;
    @SerializedName("cpu")
    private String cpu;
    @SerializedName("ram")
    private String ram;
    @SerializedName("hdd")
    private String hdd;
    @SerializedName("vga1")
    private String vga1;
    @SerializedName("vga2")
    private String vga2;
    @SerializedName("battery")
    private String battery;
    @SerializedName("weight")
    private String weight;
    @SerializedName("display")
    private String display;
    @SerializedName("other_des")
    private String otherDes;

    public String getId() {
        return id;
    }

    public String getPName() {
        return pName;
    }

    public String getPPrice() {
        return pPrice;
    }

    public String getImage() {
        return images;
    }

    public String getCName() {
        return cName;
    }

    public String getType() {
        return type;
    }

    public String getCpu() {
        return cpu;
    }

    public String getRam() {
        return ram;
    }

    public String getHdd() {
        return hdd;
    }

    public String getVga1() {
        return vga1;
    }

    public String getVga2() {
        return vga2;
    }

    public String getBattery() {
        return battery;
    }

    public String getWeight() {
        return weight;
    }

    public String getDisplay() {
        return display;
    }

    public String getOtherDes() {
        return otherDes;
    }
}
